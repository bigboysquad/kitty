# README #
it's a bot

# Command Template #
Commands are separate scripts stored in the **commands** folder. The filename is used as the command name.   

*template for commands:*
```
module.exports = {
  main: function(client, msg) {
    //command code here.
  },
  help: "Help text here!"
}
```
*To make a command admin-only, add `admin: true`.*
```
module.exports = {
  main: function(client, msg) {
    msg.channel.send('hello world');
  },
  help: "hello",
  admin: true
}
```
Random responses to messages are stored in ``commands/responses.json``.

# Config File #
The config file is at `config.json`. Most values are self-explanatory.
#Installation
download source and run `npm install`, then `node bot.js`
#Requirements
nothing right now, possibly imagemagick soon
