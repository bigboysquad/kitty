const Discord = require("discord.js");
const client = new Discord.Client();
var decache = require('decache');
var _ = require("lodash");
var fs = require('fs');
var config = require('./config.json');
var commands = {};

/* load commands from command directory */
loadCommands();
function loadCommands() {
  fs.readdirSync(config.commandDir).forEach(file => {
    if(file.endsWith('.js')) {
      var commandName = file.slice(0,-3);
      commands[commandName] = require(config.commandDir + file);
      console.log('Loaded command ' + commandName);
    }
  });
}
function checkCommand(msg, command) {
  if(command in commands) {
    if(!commands[command].admin) {
      commands[command].main(client, msg, config);
    } else if (commands[command].admin && config.admins.includes(msg.author.id)) {
      commands[command].main(client, msg, config);
    } else { msg.channel.send(':broken_heart: That command is admin-only.') }
  } else { msg.channel.send(':broken_heart: Command not found. Try ' + config.prefix + 'help for a list of commands.')}
}

/* Built-in commands */
commands.help = {};
commands.help.admin = false;
commands.help.help = "Lists commands.";
commands.help.main = function(client, msg) {
  var helpText = "``----------[HELP]----------\n";
  for(var cmd in commands) {
      helpText += (cmd + ' - ' + commands[cmd].help + '\n');
  }
  helpText += "----------[version " + config.version + "]----------``";
  msg.channel.send(helpText);
}

commands.reload = {};
commands.reload.admin = true;
commands.reload.help = "[ADMIN] Reloads installed modules.";
commands.reload.main = function(client, msg) {
  for(var cmd in commands) {
    if(cmd != "list" && cmd != "reload" && cmd != "help") {
      decache(config.commandDir + cmd + '.js');
      delete commands[cmd];
    }
  }
  loadCommands();
  msg.channel.send('``Reloaded ' + Object.keys(commands).length + ' commands.``')
}

client.on('ready', () => {
  console.log('- Kitty v' + config.version + ' logged in on ' + client.guilds.array().length + ' servers. -');
  console.log(config.startupText);
  client.user.setActivity('with bugs (version ' + config.version + ')');
});

client.on('message', msg => {
  if(msg.author.id != client.user.id) {
    var responses = JSON.parse(fs.readFileSync('./responses.json'));
    if(msg.content.startsWith(config.prefix)) {
      var command = msg.content.split(config.prefix)[1].split(' ')[0];
      checkCommand(msg, command);
    } else if (msg.content.toLowerCase() in responses) {
      msg.channel.send(responses[msg.content.toLowerCase()])
    }
  }
});
client.on('error', error => {
  console.log('[!!] ERROR [!!]\n' + error);
});
client.login(config.discordToken);
