var weekdays = ["Sunday", "Monday", "Tuesday", "Wendesday", "Thursday", "Friday", "Saturday"]
module.exports = {
	main: function(client, msg) {
		var channel = msg.member.voiceChannel;
		var date = new Date();
		hours = (date.getHours() > 12)? date.getHours() -12 : date.getHours();
		if(date.getHours() == 21 && date.getDay() == 6 && typeof channel != "undefined") {
			channel.join()
				.then(connection => {
					const dispatcher = connection.playFile('commands/assets/pianoman.mp3');
					dispatcher.on('end', function() { channel.leave() });
				});
		} else if(date.getHours() == 21 && date.getDay() == 6) { 
			msg.channel.send(':broken_heart: Join a voice channel.'); 
		} else { msg.channel.send(`It's ${hours} o'clock on a ${weekdays[date.getDay()]} (EST). This command has not unlocked yet.`) }
	},
	help: "sing me a song, piano man"
}