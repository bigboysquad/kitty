var fs = require('fs');
var responses = JSON.parse(fs.readFileSync('./responses.json'));
module.exports = {
  main: function(client, msg) {
    var args = msg.content.split(' ');
    var response = msg.content.split('"')[1];
    var trigger = msg.content.split('"')[3];
    if(args[1] == "add") {
      if(typeof response && typeof trigger != "undefined") {
        responses[trigger] = response;
        fs.writeFileSync('./responses.json', JSON.stringify(responses));
        msg.channel.send('Added response ``"' + response + "\"`` to ``" + trigger + '``');
      }
    } else if (args[1] == "remove" && typeof response != "undefined") {
      delete responses[response.toLowerCase()];
      fs.writeFileSync('./responses.json', JSON.stringify(responses));
      msg.channel.send('Removed response ``' + response + '``.');
    } else if (args[1] == "list") {
      var list = "";
      for(var obj in responses) {
        list += '``' + obj + ' - "' + responses[obj] + '"``\n';
      }
      msg.channel.send(list);
    } else { msg.channel.send(":broken_heart: ``Usage: response add [\"response\"] [\"trigger\"], response remove [\"trigger\"], or response list [trigger]``") }
  },
  help: "[ADMIN] Modify message responses. Usage: response add [\"response\"] [\"trigger\"], response remove [\"trigger\"], or response list [trigger]",
  admin: true
}
