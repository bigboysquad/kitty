module.exports = {
  main: function(client, msg) {
    var mention = msg.mentions.members.first();
    if(typeof mention != "undefined") {
      msg.channel.send(mention.user.avatarURL);
    } else { msg.channel.send(msg.author.avatarURL) }
	
  },
  help: "Gets avatar URLs"
}
