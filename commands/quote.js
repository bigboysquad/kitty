var fs = require('fs');
var quotes = JSON.parse(fs.readFileSync('./quotes.json'));
module.exports = {
  main: function(client, msg) {
    var args = msg.content.split(' ');
    var quote = msg.content.split('"')[1];
    var name = args[args.length - 1];
    if(args[1] == "add" && typeof quote != "undefined" && (!name.includes('"')))  {
      quotes[name] = quote;
      msg.channel.send('Added quote ``"' + quote + '"`` as ``' + name + "``");
      fs.writeFileSync('./quotes.json', JSON.stringify(quotes));
    } else if (args[1] == "add") {
      msg.channel.send(":broken_heart: ``Usage: quote add [\"Quote Text\"] [quotename] (no spaces in the name, please)``")
    } else if (args[1] == "remove") {
      delete quotes[args[2]];
      fs.writeFileSync('./quotes.json', JSON.stringify(quotes));
      msg.channel.send("``Deleted quote " + args[2] + ".``");
    } else {
      msg.channel.send(quotes[args[1]]);
    }
  },
  help: "View/add quotes. Usage: quote add [\"Quote Text\"] [quotename] (no spaces in the name, please) OR quote [quotename]"
}
