module.exports = {
  main: function(client, msg) {
    var id = msg.content.split(' ')[1];
    if (typeof client.channels.get(id) != "undefined") {
        var channel = client.channels.get(id);
        var everyone = msg.guild.id;
        channel.overwritePermissions(everyone, {
          SEND_MESSAGES: false,
          READ_MESSAGES: false
        }, "lock by " + msg.author.username);
        msg.channel.send('Locked #' + channel.name);
    } else {msg.channel.send(':broken_heart: `Channel ID not found. Please use the ID, not the name. You can copy channel IDs by enabling developer mode in Discord and right clicking a text channel`.')}
  },
  help: "[ADMIN] Locks channels.",
  admin: true
}
