const request = require('request');
const fs = require('fs');
module.exports = {
  main: function(client, msg, get) {
    request('https://cataas.com/cat').pipe(fs.createWriteStream('cat.png').on('finish', () => {
      msg.channel.send({ files: ['cat.png'] });
    }));
  },
  help: "Sends cat pics."
}
