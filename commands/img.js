var request = require('request');
module.exports = {
  main: function(client, msg, config) {
	var q = msg.content.substr(msg.content.indexOf(' ')+1);
	var url = "https://www.googleapis.com/customsearch/v1?key=" + config.googlesearch_apikey + "&cx=" + config.googlesearch_id + "&searchType=image&q="; 
	request(url + q, function (error, response, body) {
		var res = JSON.parse(body);
		var img = res.items[0].link;
		var responseMsg = "``Note: image searches are limited to 100 queries per day. blame google. \nImage search results for " + q + ":`` \n" + img;
		msg.channel.send(responseMsg);
	});		
  },
  help: "Search Google Images."
}
