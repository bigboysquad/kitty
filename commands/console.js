module.exports = {
  main: function(client, msg, config) {
    if(msg.author.id != client.user.id) {
      var command = msg.content.split(config.prefix + 'console ')[1].split(' ')[0];
      console.log(command);
    }
  },
  help: "Sends a message to the developer's command console",
  admin: true
}
