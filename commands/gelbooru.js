var request = require('request');
module.exports = {
  main: function(client, msg) {
    var tags = msg.content.substr(msg.content.indexOf(' ')+1);
    msg.channel.send('``Top result for ' + tags + ':``');
    request('https://gelbooru.com/index.php?page=dapi&s=post&q=index&json=1&tags=' + tags + " sort:score", function (error, response, body) {
      console.log(body);
      console.log('error:', error);
      try {
        json = JSON.parse(body);
        console.log(json[0])
        msg.channel.send(json[0].file_url);
      }
      catch(error) {
        msg.channel.send('``Error searching. Tags are probably invalid.``')
      }
    });
  },
  help: "Searches Gelbooru."
}
