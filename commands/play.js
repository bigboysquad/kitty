const ytdl = require('ytdl-core');
const streamOptions = { seek: 0, volume: 1 };
var playing;
module.exports = {
  main: function(client, msg) {
    var args = msg.content.split(' ');
    var channel = msg.member.voiceChannel;
    if(!playing && typeof channel != "undefined") {
      channel.join()
        .then(connection => {
          const stream = ytdl(args[1], { filter : 'audioonly' });
          const dispatcher = connection.playStream(stream, streamOptions);
          playing = true;
          dispatcher.on('end', function() {
            channel.leave();
            playing = false;
          });
        });
      } else if(!playing) {
      	  msg.channel.send(':broken_heart: Please join a voice channel.');
    	} else {
        msg.channel.send(':broken_heart: Audio is already being played.');
      }
  },
  help: "Plays audio from YouTube."
}
