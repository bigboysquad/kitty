module.exports = {
  main: function(client, msg) {
      var sayings = ["It is certain",
										"It is decidedly so",
										"Without a doubt",
										"Yes, definitely",
										"You may rely on it",
										"As I see it, yes",
										"Most likely",
										"Outlook good",
										"Yes",
										"Signs point to yes",
                    "In a minute",
                    "How many times do I have to tell you \"no\"?",
                    "Not until you finish your vegetables",
                    "You\'ll poke your eye out",
                    "I\'ll give you something to cry about",
                    "You\'re adopted",
                    "Try it, you might like it",
                    "Burger king foot lettuce",
										"Ask again later",
										"Better not tell you now",
										"Cannot predict now",
										"Concentrate and ask again",
										"Don't count on it",
										"My reply is no",
										"My sources say no",
										"Outlook not so good",
										"Very doubtful"];

			var result = Math.floor((Math.random() * sayings.length) + 0);
			msg.channel.send(sayings[result]);
  },
  help: "It\'s an 8-ball. That\'s it"
}
