var playing;
module.exports = {
  main: function(client, msg) {
    var args = msg.content.split(' ');
    var channel = msg.member.voiceChannel;
    if(!playing && typeof channel != "undefined") {
      channel.join()
        .then(connection => {
          const dispatcher = connection.playFile('commands/assets/laughtrack.wav');
          playing = true;
          dispatcher.on('end', function() {
            channel.leave();
            playing = false;
          });
        });
      }
  },
  help: "Plays audio from YouTube."
}
