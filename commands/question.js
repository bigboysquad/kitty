var request = require('request');
var fs = require('fs');
module.exports = {
  main: function(client, msg, config) {
  	var q = msg.content.substr(msg.content.indexOf(' ')+1);
  	msg.channel.send('Calculating..');
  	msg.channel.startTyping();
  	var req = request("https://api.wolframalpha.com/v2/query?input=" + q + "&output=json&appid=" + config.wolfram_apikey, function(error, response, body) {
      var res = JSON.parse(body);
      if(res.queryresult.success) {
        msg.channel.send(`Result:\n\`\`\`${res.queryresult.pods[1].subpods[0].plaintext}\`\`\``);
        msg.channel.stopTyping();
      } else { 
        msg.channel.send(':broken_heart: ¯\\_(ツ)_/¯');
        msg.channel.stopTyping();
      }
    });

  },
  help: "Let Kitty answer your questions!2"
}
