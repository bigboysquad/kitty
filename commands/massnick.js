module.exports = {
  main: function(client, msg) {
    var nick = msg.content.substr(msg.content.indexOf(' ')+1);
    msg.guild.members.forEach(function(member) {
      member.setNickname(nick, "kitty")
        .catch(function(error) {
          msg.channel.send('``` :broken_heart: massnick error: ' + error + '```');
        });
    });
    msg.channel.send("Changing " + msg.guild.members.size  + " usernames. It might take a while for large servers.");
  },
  help: "Changes nickname of everyone in the server.",
  admin: true
}
