module.exports = {
  main: function(client, msg) {
    var id = msg.content.split(' ')[1];
    if(typeof msg.channel.fetchMessage(id) != "undefined") {
      var message = msg.channel.fetchMessage(id)
      .then(message => message.pin());
    } else {
      msg.channel.send(':broken_heart: Message ID not found. Enable developer mode to copy message IDs.');
    }
  },
  help: "Pins messages"
}
